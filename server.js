const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});



// Sa se implementeze metode de tip PUT si DELETE pentru update-ul de produse in functie de id si stergerea de produse in functie de denumire.
// (Note: pentru PUT se va transmite id-ul ca si path param iar pentru DELETE se va transmite numele produsului ca si payload in body);

app.put('/update/:id',(req,res)=>{
   let x=req.params.id;
   
   products.forEach((product)=>{
       if(product.id==x){
           product.productName=req.body.productName;
           product.price=req.body.price;
       }
   });
   res.status(200).send('The prod has been changed!')
    
})



app.delete('/delete/:productName',(req,res)=>{
    
    products.forEach((product)=>{
        
        
    });
    
    
    
})














app.listen(8080, () => {
    console.log('Server started on port 8080...');
});